﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour {

	public Vector2 velocity; // in metres per second
	public Vector2 move;
	public float maxSpeed = 5.0f;

	public float acceleration = 3.0f; // in metres/second/second
	private float speed = 0.0f;    // in metres/second
	public float brake = 5.0f; // in metres/second/second
	public float turnSpeed = 30.0f; // in degrees/second

	public float destroyRadius = 1.0f;

	private BeeSpawner beeSpawner;

	void Start() {
		// find the bee spawner and store a reference for later
		beeSpawner = FindObjectOfType<BeeSpawner> ();
	}



	void Update() {

		if (Input.GetButtonDown ("Fire1")) {
			// destroy nearby bees
			beeSpawner.DestroyBees(
				transform.position, destroyRadius);
		}

		Vector2 direction;
		if (gameObject.name == "Player") {
			direction.x = Input.GetAxis("Horizontal");
			direction.y = Input.GetAxis("Vertical");
		} else {
			direction.x = Input.GetAxis("Horizontal2");
			direction.y = Input.GetAxis("Vertical2");
		}

		// scale by the maxSpeed parameter
		Vector2 velocity = direction * maxSpeed;
		// scale the velocity by the frame duration
		Vector2 move = velocity * Time.deltaTime;

		// move the object
		transform.Translate(move);
	}

	void Update1() {
		// the horizontal axis controls the turn
		float turn = Input.GetAxis("Horizontal");

		// turn the car
		//turnSpeed += speed;
		transform.Rotate(0, 0, -turn * turnSpeed * speed * Time.deltaTime);

		// the vertical axis controls acceleration fwd/back
		float forwards = Input.GetAxis("Vertical");
		if (forwards > 0) {
			// accelerate forwards
			speed = speed + acceleration * Time.deltaTime;
		}
		else if (forwards < 0) {
			// accelerate backwards
			speed = speed - acceleration * Time.deltaTime;
		}

		else {
			// braking
			brake = Mathf.Clamp(brake, -maxSpeed, maxSpeed);
			if (speed > 0) {
				speed = speed - brake * Time.deltaTime;
			} else {
				speed = speed + brake * Time.deltaTime;
			}
		}

		// clamp the speed
		speed = Mathf.Clamp(speed, -maxSpeed, maxSpeed);

		// compute a vector in the up direction of length speed
		Vector2 velocity = Vector2.up * speed;

		// move the object
		transform.Translate(velocity * Time.deltaTime, Space.Self);
	}


}