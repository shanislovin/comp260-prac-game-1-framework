﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeSpawner : MonoBehaviour {
	public float beePeriod;
	public float minBeePeriod = 1;
	public float maxBeePeriod = 4;
	private float count;

	public int nBees = 0;
	public int maxBees = 30;
	public BeeMove beePrefab;
	public Rect spawnRect;
	//public PlayerMove player;
	// the spawn rectangle
	public float xMin, yMin;
	public float width, height;

	void OnDrawGizmos() {
		// draw the spawning rectangle
		Gizmos.color = Color.green;
		Gizmos.DrawLine(
			new Vector2(spawnRect.xMin, spawnRect.yMin), 
			new Vector2(spawnRect.xMax, spawnRect.yMin));
		Gizmos.DrawLine(
			new Vector2(spawnRect.xMax, spawnRect.yMin), 
			new Vector2(spawnRect.xMax, spawnRect.yMax));
		Gizmos.DrawLine(
			new Vector2(spawnRect.xMax, spawnRect.yMax), 
			new Vector2(spawnRect.xMin, spawnRect.yMax));
		Gizmos.DrawLine(
			new Vector2(spawnRect.xMin, spawnRect.yMax), 
			new Vector2(spawnRect.xMin, spawnRect.yMin));
	}


	// Use this for initialization
	void Start () {
		beePeriod = Random.Range (minBeePeriod, maxBeePeriod);
	}

	// Update is called once per frame
	void Update () {
		if (count >= beePeriod) {
			if (nBees < maxBees) {
				SpawnBee ();
				count = 0;
				beePeriod = Random.Range (minBeePeriod, maxBeePeriod);
			}
		}
		count += Time.deltaTime;
		//OnDrawGizmos ();
	}

	void SpawnBee () {
		// instantiate a bee
		int beeCount=1;
		BeeMove bee = Instantiate (beePrefab);

		// attach to this object in the heirarchy
		bee.transform.parent = transform;
		// give the bee a name and number
		bee.gameObject.name = "Bee " + beeCount;

		// move the bee to a random position within 
		// the spawn rectangle
		float x = spawnRect.xMin + Random.value * spawnRect.width;
		float y = spawnRect.yMin + Random.value * spawnRect.height;

		bee.transform.position = new Vector2 (x, y);
		nBees++;
		beeCount++;

	}

	public void DestroyBees(Vector2 centre, float radius) {
		// destroy all bees within ‘radius’ of ‘centre’
		for (int i = 0; i < transform.childCount; i++) {
			Transform child = transform.GetChild(i);
			// Fixed bug by adding a type conversion
			Vector2 v = (Vector2)child.position - centre;
			if (v.magnitude <= radius) {
				Destroy(child.gameObject);
			}
		}
	}

}
