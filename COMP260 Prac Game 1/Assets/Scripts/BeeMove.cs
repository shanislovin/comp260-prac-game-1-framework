﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BeeMove : MonoBehaviour {
	public Vector2 direction;
	public Vector2 direction1;

	// public parameters with default values
	public float minSpeed, maxSpeed;
	public float minTurnSpeed, maxTurnSpeed;

	// private state
	private float speed;
	private float turnSpeed;

	//private Transform target;
	private Transform target;
	private Vector2 heading;

	public ParticleSystem explosionPrefab;

	void OnDestroy() {
		// create an explosion at the bee's current position
		ParticleSystem explosion = Instantiate(explosionPrefab);
		explosion.transform.position = transform.position;
		// destroy the particle system when it is over
		Destroy(explosion.gameObject, explosion.duration);
	}


	// Use this for initialization
	void Start () {
		// find the player to set the target
		//PlayerMove p = FindObjectOfType<PlayerMove>();
		//target = p.transform;

		// bee initially moves in random direction
		heading = Vector2.right;
		float angle = Random.value * 360;
		heading = heading.Rotate(angle);

		// set speed and turnSpeed randomly 
		speed = Mathf.Lerp(minSpeed, maxSpeed, Random.value);
		turnSpeed = Mathf.Lerp(minTurnSpeed, maxTurnSpeed, 
			Random.value);

		// find a player object to be the target by type
		// Note: this is not standard Unity syntax
		PlayerMove player = FindObjectOfType<PlayerMove>();
		target = player.transform;

	}
		


	// Update is called once per frame
	void Update() {
		// get the vector from the bee to the target 
		direction1 = target.position - transform.position;
		//direction2 = target2.position - transform.position;

		//if (direction1.magnitude < direction2.magnitude)
		direction = direction1;

		// calculate how much to turn per frame
		float angle = turnSpeed * Time.deltaTime;

		// turn left or right
		if (direction.IsOnLeft(heading)) {
			// target on left, rotate anticlockwise
			heading = heading.Rotate(angle);
		}
		else {
			// target on right, rotate clockwise
			heading = heading.Rotate(-angle);
		}
		transform.Translate(heading * speed * Time.deltaTime);

	}
}
